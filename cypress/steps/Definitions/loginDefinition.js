    
import {Given, When, Then, And} from 'cypress-cucumber-preprocessor/steps'
import loginPage from '../Methods/loginPage'
import mainPage from '../Methods/mainPage'

Given("I am on the {string} page", (pageName) => {
    mainPage.gotoPage(pageName)});

And("I am already logged in", () => {
    loginPage.customerLogin()});

//------------------------------------------------------------------------------------------------

When("I try to login with a valid account", () => {
    loginPage.customerLogin()});

When("I am disconnecting", () => {
    loginPage.customerDisconnect()});

When("I try to login with empty fields", () => {
    loginPage.customerEmptylogin()});

When("I try to login with invalid credentials", () => {
    loginPage.customerBadlogin()});

When("I try to login with an unactivated email", () => {
    loginPage.customerLoginMailNotActivated()});

When("I am clicking on the account creation button", () => {
    loginPage.customerAccessAccountCreation()});

When("I am creating a new account", () => {
    loginPage.newAccountCreation()});

When("I am creating a new account with bad credentials", () => {
    loginPage.newAccountCreationbadCredentials()});

//------------------------------------------------------------------------------------------------

Then("I am logged in", () => {
    loginPage.checkCustomerSuccessfulLogin()});

Then("I am disconnected", () => {
    loginPage.checkCustomerIsDisconnected()});


Then("I can see the errors on my empty login fields", () => {
    loginPage.checkCustomerEmptyLoginErrors()});

Then("I am can see the login error message", () => {
    loginPage.checkCustomerBadLoginError()});

Then("I am can see the unactivated mail page", () => {
        loginPage.checkCustomerEmailNotActivated()});

Then("I am redirected to the account creation form", () => {
    mainPage.checkPageURL("https://customeridentity.younited-credit.com/Fr/Account/CreateNewAccount")});

Then("I can see my account had been created", () => {
    loginPage.checkNewAccountCreationSuccessful()});

Then("I can see errors on the account creation page", () => {
    loginPage.checkAccountCreationEmailInvalid()});
