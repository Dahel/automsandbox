import {Given, When, Then, And} from 'cypress-cucumber-preprocessor/steps'
import mainPage from '../Methods/mainPage'

When("I go to the page {string}", (pageURL) => {
    mainPage.gotoURL(pageURL);});
Then("I can see the page url is {string}", (pageURL) => {
    mainPage.checkPageURL(pageURL);});