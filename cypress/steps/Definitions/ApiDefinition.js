import {Given, When, Then, And} from 'cypress-cucumber-preprocessor/steps'
import ApiPage from '../Methods/ApiPage'

Given("I am logged in", () => {
    ApiPage.setUserLogin()})

Given("I load my data files", () => {
    ApiPage.setVariableFromFixture()})

And("I check my cart",() => {
    ApiPage.getAllCartProducts()})

And("I have a product in my cart", () => {
    ApiPage.setProductInCart()})

When("I register a new user with mail:{string}", (email) => {
    ApiPage.registerNewUser(email)})

When("I login with my user {string}", (email) => {
    ApiPage.loginUser(email)})

When("I create a new product: {string}, {string}, {string}, {int}", (productName, productDesc, productImgUrl, productPrice) => {
    ApiPage.createProduct(productName, productDesc, productImgUrl, productPrice)})

When("I add a product to my cart", () => {
    ApiPage.addProductToCart(1)})

When("I update my product quantity by {int}", (quantity) => {
    ApiPage.updateCartProductQty(quantity)})

When("I delete a product from my cart", () => {
    ApiPage.deleteSingleCartProduct()})

When("I delete all products from my cart", () => {
    ApiPage.deleteAllCartProduct()})

When("I complete my order", () => {
    ApiPage.createOrder()})

When("I add a comment {string} to my current product", (comment) => {
    ApiPage.addProductComment(comment)})

When("I add a product to my cart", () => {
    ApiPage.addProductToCart()})

Then("I can see my user {string} is registered", (checkEmail) => {
    ApiPage.checkUserRegistration(checkEmail)})

Then("I can see my user is already registered", () => {
    ApiPage.checkUserAlreadyExist()})

Then("I can see my user is logged in", () => {
    ApiPage.checkUserLogin()})

Then("I can see my user is not logged in", () => {
    })

Then("I can see my product {string} has been created", (checkProduct) => {
    ApiPage.checkProductCreation(checkProduct)})

Then("I can see my comment {string} has been added", (checkComment) => {
    ApiPage.checkCommentAdded(checkComment)})

Then("my product is added to my cart", () => {
    ApiPage.checkProductAddedToCart()})

Then("I can see my product quantity has been updated", () => {
    ApiPage.checkQtyUpdated()})

Then("my product has been removed from my cart", () => {
    ApiPage.checkProductRemovedFromCart()})

Then("my cart is now empty", () => {
    ApiPage.checkCartIsEmpty();})

Then("all my products have been deleted from my cart", () => {
    ApiPage.checkCartDeleteAll()})

Then("my order is completed", () => {
    ApiPage.checkOrderSuccessful()})

Then("my order completion failed", () => {
    ApiPage.checkOrderFailed()})

Then("I see that my product already exists", () => {
    ApiPage.checkProductAlreadyExist()})

Then("I see the following response: {int} {string}", (error, message) => {
    ApiPage.checkProductCreationKO(error, message)})