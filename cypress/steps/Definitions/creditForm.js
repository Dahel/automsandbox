import {Given, When, Then, And} from 'cypress-cucumber-preprocessor/steps'
import creditForm from '../Methods/creditFormPage'

When("I am asking a new credit of {int} € for a {string}", (amount, project) => {
    creditForm.startSimulator(project, amount)});

When("I am asking a new credit of {int} € for a {string} for a duration of {int} months", (amount, project, duration) => {
    creditForm.startSimulator(project, amount, duration)});

When('I am filling the credit simulation form', () => {
    creditForm.fillFormData()});

When ("I am making a new credit simulation with the following informations: {string}, {int}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {int}, {string}, {string}, {string}, {string}, {string}, {string}, {int}, {int}, {int}, {int}, {string}, {string}", (Projet, Somme, Duree, Titre, Nom, Prénom, NomJeuneFille, Email, Situation, NbEnfants, Logement, LogeDepuis, Emploi, Secteur, Profession, EmploiDepuis, Salaire, AutreRevenu, LoyerCredit, ChargesEnfant, Bank, BankDepuis) => {
    creditForm.fillFormData(Projet, Somme, Duree, Titre, Nom, Prénom, NomJeuneFille, Email, Situation, NbEnfants, Logement, LogeDepuis, Emploi, Secteur, Profession, EmploiDepuis, Salaire, AutreRevenu, LoyerCredit, ChargesEnfant, Bank, BankDepuis)});

Then("I can see my credit simulation is {string}", (isSuccessful) => {
    creditForm.checkCreditSimulationSuccessful(isSuccessful)})