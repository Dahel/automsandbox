var userObj = {st:"empty"}
var productObj = {st:"empty"}
var creditCard = {st:"empty"}

var APIResponse ="empty"
var loginToken ="not set";

export default {

//----------------------------API Call----------------------------------------------

    setVariableFromFixture() {
        cy.fixture('userData.json').then((userData) => {userObj = userData.userList[0]; cy.log(userObj)})
        cy.fixture('productData.json').then((productData) => {productObj = productData.productList[0]; cy.log(productObj)})
        cy.fixture('cardData.json').then((cardData) => {creditCard = cardData.cardList[0]; cy.log(creditCard)})
    },

    setUserLogin() {
        if(loginToken == "not set") {
        this.loginUser(userObj.email)};
        cy.log(loginToken)
        cy.log(userObj);
        cy.log(productObj);
},

    setProductInCart(){
        this.addProductToCart(1);
        cy.log("product added to cart")
        cy.log(productObj)
    },


    registerNewUser(email){ 
        cy.request({
        method: 'POST',
        headers: {"Content-Type": "application/json"},
        url: 'https://ztrain-shop.herokuapp.com/user/register',
        failOnStatusCode: false,
        body: {
            email: email,
            password: "Azerty00",
            adress: "10 place de la république",
            age: 25
        }
        }).then((response) => {
            APIResponse = response;
    })},

 //   setCurrentUser(setObj){
 //       userObj = setObj;
 //       cy.log(userObj);
 //   },

 //   setCurrentProduct(setObj){
 //       productObj = setObj;
 //       cy.log(productObj);
 //   },

    loginUser(email){ 

        cy.request({
        method: 'POST',
        headers: {"Content-Type": "application/json"},
        url: 'https://ztrain-shop.herokuapp.com/auth/login/',
        failOnStatusCode: false,
        body: {
            email: email,
            password: "azerty00",
        }
        }).then((response) => {
            if(response.status == 201) {
                loginToken = "".concat("Bearer ", response.body.token);
            }
        })

    },

    createProduct(productName, productDesc, productImgUrl, productPrice){ 
        cy.request({
        method: 'POST',
        headers: {"Content-Type": "application/json",
                  "Authorization": loginToken},
        url: 'https://ztrain-shop.herokuapp.com/product/create',
        failOnStatusCode: false,
        body: {
            name: productName,
            description: productDesc,
            image: productImgUrl,
            price: productPrice,
            isActive: false
        }
        }).then((response) => {
            APIResponse = response;
            cy.log(response);
        });
    },

    addProductToCart(qty){ 
        cy.log(productObj)
        cy.request({
        method: 'POST',
        headers: {"Content-Type": "application/json",
                  "Authorization": loginToken},
        url: 'https://ztrain-shop.herokuapp.com/cart/add',
        failOnStatusCode: false,
        body: {
            product: productObj._id,
            user_id: userObj._id,
            quantity: qty
        }
        }).then((response) => {
            APIResponse = response;
    })},

    updateCartProductQty(qty){ 
        cy.request({
        method: 'PUT',
        headers: {"Content-Type": "application/json",
                  "Authorization": loginToken},
        url: 'https://ztrain-shop.herokuapp.com/cart/update',
        failOnStatusCode: false,
        body: {
            product: productObj._id,
            user_id: userObj._id,
            quantity: qty
        }
        }).then((response) => {
            APIResponse = response;
    })},

    
    deleteSingleCartProduct(){ 
        cy.request({
        method: 'DELETE',
        headers: {"Content-Type": "application/json",
                  "Authorization": loginToken},
        url: 'https://ztrain-shop.herokuapp.com/cart/delete',
        failOnStatusCode: false,
        body: {
            product: productObj._id,
            user_id: userObj._id,
        }
        }).then((response) => {
            APIResponse = response;
    })},

    deleteAllCartProduct(){ 
        cy.request({
        method: 'DELETE',
        headers: {"Content-Type": "application/json",
                  "Authorization": loginToken},
        url: 'https://ztrain-shop.herokuapp.com/cart/delete/' + userObj._id,
        failOnStatusCode: false,
        body: {}
        }).then((response) => {
            APIResponse = response;
    })},

    getAllCartProducts(){ 
        cy.request({
        method: 'GET',
        headers: {"Content-Type": "application/json",
                  "Authorization": loginToken},
        url: 'https://ztrain-shop.herokuapp.com/cart/' + userObj._id,
        body: {}
        }).then((response) => {
            APIResponse = response;
            cy.log(APIResponse);
    })},

    createOrder(){ 
        cy.request({
        method: 'POST',
        headers: {"Content-Type": "application/json",
                  "Authorization": loginToken},
        url: 'https://ztrain-shop.herokuapp.com/command/create',
        failOnStatusCode: false,
        body: {
            user_id: userObj._id,
            address: userObj.adress,
            card: {
                  number: creditCard.number,
                  exp_month: creditCard.exp_month,
                  exp_year: creditCard.exp_year,
                  cvc: creditCard.cvc
            }
        }
        }).then((response) => {
            APIResponse = response;
    })},

    addProductComment(comment){ 
        cy.request({
        method: 'POST',
        headers: {"Content-Type": "application/json",
                  "Authorization": loginToken},
        url: 'https://ztrain-shop.herokuapp.com/product/comments/add',
        failOnStatusCode: false,
        body: {
            product_id: productObj._id,
            user_id: userObj._id,
            message: comment
        }
        }).then((response) => {
            APIResponse = response;
            cy.log(APIResponse);
    })},

//----------------------------Check response-------------------------------------------------

    checkUserRegistration(checkEmail) {
        expect(APIResponse.status).to.eq(201)
        expect(APIResponse.body.user.email).to.eq(checkEmail);
    },

    checkUserAlreadyExist(){
        expect(APIResponse.status).to.eq(400)
        expect(APIResponse.body.message).to.eq("user already exists");
    },

    checkUserLogin() {
        cy.log(APIResponse);
        expect(loginToken).to.not.equal('not set');
    },

    checkProductCreation(checkProduct){
        cy.log(APIResponse);
        expect(APIResponse.status).to.eq(201);
        expect(APIResponse.body.name).to.eq(checkProduct);
    },

    checkProductAlreadyExist(){
        cy.log(APIResponse);
        expect(APIResponse.status).to.eq(403);
        expect(APIResponse.body.error).to.eq("Ce Produit existe déja");
    },

    checkProductCreationKO(error, message) {
        cy.log(APIResponse);
        expect(APIResponse.status).to.eq(error);
        if(error == 403) {expect(APIResponse.body.error).to.eq(message);}
        else {expect(APIResponse.body.message[0]).to.eq(message);}
    },


    checkCommentAdded(checkComment) {
        cy.log(APIResponse);
        expect(APIResponse.status).to.eq(201);
        expect(APIResponse.body.message).to.eq(checkComment);
    },

    checkProductAddedToCart(){
        cy.log(APIResponse);
        expect(APIResponse.status).to.eq(201);
        expect(APIResponse.body.message).to.eq("Votre panier à été mis à jour");
    },

    checkProductRemovedFromCart() {
        cy.log(APIResponse);
        expect(APIResponse.status).to.eq(200);
        expect(APIResponse.body.message).to.eq("product remove cart successfully");
    },

    checkQtyUpdated() {
        cy.log(APIResponse);
        expect(APIResponse.status).to.eq(200);
        expect(APIResponse.body.message).to.eq("Votre panier à été mis à jour");
    },

    checkCartIsEmpty(){
        cy.log(APIResponse);
        expect(APIResponse.status).to.eq(200);
        expect(APIResponse.body.length).to.eq(0);
    },

    checkCartDeleteAll(){
        cy.log(APIResponse);
        expect(APIResponse.status).to.eq(200);
        expect(APIResponse.body.message).to.eq("remove all product cart successfully");
    },

    checkOrderSuccessful(){
        cy.log(APIResponse);
        expect(APIResponse.status).to.eq(201);
        expect(APIResponse.body.message).to.eq("Bravo!!! votre commande a été validé");
    },

    checkOrderFailed() {
        expect(APIResponse.status).to.eq(403);
        expect(APIResponse.body.error).to.eq("Votre panier est vide");
    }
}

