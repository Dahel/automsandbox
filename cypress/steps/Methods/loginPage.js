export default {

    customerLogin(){

        cy.fixture('projectConst').then((dataset) => {
            cy.get('input#email').type(dataset.customerMail);       
            cy.get('input#Password').type(dataset.customerPassword);
        })
        cy.get('button#submit-button').click();
    },

    customerLoginMailNotActivated() {
        cy.get('a#createMagicLink').click();
        cy.fixture('projectConst').then((dataset) => {
            cy.get('input#email').type(dataset.customerDeactivatedMail)});
            cy.get('button#submit-button').click();     
},

    customerEmptylogin(){
        cy.get('input#email').clear();
        cy.get('input#Password').clear();
        cy.get('button#submit-button').click();  
    },

    customerBadlogin(){
        cy.get('input#email').type("abc@def.gh");;
        cy.get('input#Password').type("Azertyuiop");
        cy.get('button#submit-button').click();     
    },

    customerDisconnect() {
        cy.get('yuc-header > button').click()   
    },

    customerAccessAccountCreation(){
        cy.get('a#createNewAccount').click()
    },

    newAccountCreation(){
        cy.get('input#email').type('abc@def.fr');
        cy.get('input#cellPhone').type('0607080910');
        cy.get('button#submit-button').click();   
    },
    
    newAccountCreationbadCredentials(){
        cy.get('input#email').type('azerty');
        cy.get('input#cellPhone').type('12345678910');
        cy.get('button#submit-button').click();   
    },
    
//--------------------------------------------------------------------------------------

    checkCustomerEmptyLoginErrors(){
        cy.get('span[data-valmsg-for="Email"]')
        .should('be.visible')
        .contains("Enter an email address");

        cy.get('span[data-valmsg-for="Password"]')
        .should('be.visible')
        .contains("Enter a password (min. 10 characters)");
    },

    checkCustomerEmailNotActivated() {
        cy.get('form[action="ConfirmEmail"]')
        .should('be.visible')
        .contains("Confirmez votre adresse email pour accéder à votre compte !")

        cy.get('button[type="submit]')
        .should('have.text',"Recevoir l'email de confirmation")
    },

    checkCustomerBadLoginError(){
        cy.get('div.error-msg')
        .should('be.visible')
        .contains("Adresse e-mail ou mot de passe invalide. Réessayez ou cliquez sur \"Mot de passe oublié\" pour le réinitialiser.");
    },

    checkCustomerSuccessfulLogin(){
        cy.get('yuc-applications').should('be.visible');
        cy.fixture('projectConst').then((dataset) => {
            cy.get('yuc-header > small').should('have.text',dataset.customerMail);
        })
    },

    checkCustomerIsDisconnected(){
        cy.visit('https://account.younited-credit.com/fr');
        cy.get('form.form-login').should('be.visible')
        cy.url().should('contain',"https://customeridentity.younited-credit.com")
    },

    checkCustomerAccountNotActivated(){
        cy.get('form[action="ConfirmEmail"]').should('be.visible');
    },

    checkNewAccountCreationSuccessful(){
        cy.get('p.heading--small')
        .should('be.visible')
        .contains('Un email vous a été envoyé');
    },

    checkAccountCreationEmailInvalid(){
        cy.get('span#email-error')
        .should('be.visible')
        .contains('Please enter a valid email address')
    }

}