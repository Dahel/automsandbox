var relStatusMod = "";
var houseStatusMod = "";
var proSituationMod = "";
var childNumMod = "";

export default {

secondFormNextStep() {
    cy.wait(500);
    cy.get('button[data-test="navigator-compact-forward"]').click({force: true});
},

secondFormPrevStep() {
    cy.get('button[data-test="navigator-back-button"]').click();
},

startSimulator(project,amount, duration = 0) {

    var amountStr = amount.toString().concat(" €");
    var durationStr = duration.toString().concat(" mois");

    cy.get('select#projectSelect').first().select(project);
    cy.get('select#amount').first().select(amountStr);

    if(duration != 0) {
        cy.get('select#creditMaturity').first().select(durationStr);
        cy.log("DURATION SET")}
    cy.get('a[title="Continuer"]').first().click();
},

//-----------------------------------FIRST FORM----------------------------------------------------------
    firstFormFill(email, relStatus) {
        relStatusMod = relStatus;
    cy.get('div[data-cy="email-title"]').should('be.visible');
    cy.get('input').type(email);
    cy.wait(2000);
    cy.get('button').contains('Voir mon offre personnalisée').click();

    cy.get('div[data-cy="marital-title"]').should('be.visible')
    cy.get('label.card-choice').contains(relStatus).click();
    cy.get('yuc-design-button[data-cy="next"]').click();
    cy.wait(3000)
    },


//------------------------------------SECOND FORM---------------------------------------------------------

secondFormEmailStep(email) {
    cy.get('input#email-input').clear().type(email);
    cy.wait(2000);
},

secondFormSituationStep(childNum =0){
    if (childNum >= 9) {
        childNumMod ="+";
    }
    else {
        childNumMod = childNum.toString();
    }
        cy.get('select#maritalStatus-input').select(relStatusMod);
        cy.get('select#childNumberPropal-input').select(childNumMod);
        cy.get('label[for="yucDataManagementOptin-input"]').click('topLeft');
},

secondFormHousingStep(houseStatus, sinceDate){

    var sinceMonth = sinceDate.split('/')[0];
    var sinceYear = sinceDate.split('/')[1];

        houseStatusMod = houseStatus;
        cy.get('select#housingStatus-input').select(houseStatus);
        cy.get('input#housingStatusFrom-input-month').type(sinceMonth);
        cy.get('input#housingStatusFrom-input-year').type(sinceYear);
},
secondFormProfessionStep(proSituation, sector = "" , profession = "", sinceDate = "", isBankrupt = "non") {

    var sinceMonth = sinceDate.split('/')[0];
    var sinceYear = sinceDate.split('/')[1];
    proSituationMod = proSituation;

    var sectorLocator = ""
    if (sector == "Public") {sectorLocator = 'label[for="SECTOR_PUBLIC_SECTOR"]'};
    if (sector == "Privé") {sectorLocator = 'label[for="SECTOR_PRIVATE_SECTOR"]'};


    cy.get('select#professionalSituation-input').select(proSituation);


    switch(proSituation) {
        case "CDI":
        case "CDD":
        case "Intérimaire":
            cy.get(sectorLocator).click();
            cy.get('select#profession-input').select(profession);
            cy.get('input#employedFrom-input-month').type(sinceMonth);
            cy.get('input#employedFrom-input-year').type(sinceYear);
            break;
        case "Indépendants / Travailleurs non salariés": 
            cy.get('select#profession-input').select(profession);
            cy.get('input#businessActivityStartDate-input-month').type(sinceMonth);
            cy.get('input#businessActivityStartDate-input-year').type(sinceYear);
            if(isBankrupt == "oui") {cy.get('label[for="ISCOMPANYBANKRUPT_TRUE"]').click();}
            if(isBankrupt == "non") {cy.get('label[for="ISCOMPANYBANKRUPT_FALSE"]').click();}
            break;
        case "Pensionnés / Retraités": 
            cy.get('select#profession-input').select("Retraité");
            cy.get('input#pensionFrom-input-month').type(sinceMonth);
            cy.get('input#pensionFrom-input-year').type(sinceYear);
            break;
        case "Etudiant":
        case "Sans profession – Sans emploi": ;break;
    };
},
//------------------------------------------


secondFormPartnerProfessionStep(proSituation, sector = "" , profession = "", sinceDate = "", isBankrupt = "non") {

    var sinceMonth = sinceDate.split('/')[0];
    var sinceYear = sinceDate.split('/')[1];

    var sectorLocator = ""
    if (sector == "public") {sectorLocator = 'label[for="PARTNERSECTOR_PUBLIC_SECTOR"]'};
    if (sector == "privé") {sectorLocator = 'label[for="PARTNERSECTOR_PRIVATE_SECTOR"]'};


    cy.get('select#partnerProfessionalSituation-input').select(proSituation);


    switch(proSituation) {
        case "CDI":
        case "CDD":
        case "Intérimaire":
            cy.get(sectorLocator).click();
            cy.get('select#partnerProfession-input').select(profession);
            cy.get('input#partnerEmployedFrom-input-month').type(sinceMonth);
            cy.get('input#partnerEmployedFrom-input-year').type(sinceYear);
        case "Indépendants / Travailleurs non salariés": 
            cy.get('select#partnerProfession-input').select(profession);
            cy.get('input#partnerBusinessActivityStartDate-input-month').type(sinceMonth);
            cy.get('input#partnerBusinessActivityStartDate-input-year').type(sinceYear);
            if(isBankrupt == "oui") {cy.get('label[for="PARTNERISCOMPANYBANKRUPT_TRUE"]').click();}
            if(isBankrupt == "non") {cy.get('label[for="PARTNERISCOMPANYBANKRUPT_FALSE"]').click();}
            break;
        case "Pensionnés / Retraités": 
            cy.get('input#partnerPensionFrom-input-month').type(sinceMonth);
            cy.get('input#partnerPensionFrom-input-year').type(sinceYear);
            break;
        case "Etudiant":
        case "Sans profession – Sans emploi": ;break;
    };
},


secondFormIncomeStep(salary, partnerSalary = 0, houseIncome = 0, addIncome = 0) {
    if(proSituationMod)
    cy.get('input#mainIncome-input').type(salary);
    cy.get('input#housingAssistance-input').type(houseIncome);
    cy.get('input#additionalIncome-input').type(addIncome);
    if(relStatusMod == "Marié(e)"|| relStatusMod == "Vie Maritale / PACS") {
        cy.get('input#coIncome-input').type(partnerSalary);
    }


//  cy.get('span[data-test="totalIncome"]').should('contain', salary + partnerSalary + houseINcome + addIncome);
},

secondFormRentMortgageStep(rentOrMortgage =0 , suppPayment =0, childExp =0,mortgageNum =0, mortgageType ="", mortgageAmount =0){
    var mortgageNumMod
    if(mortgageNum >= 6) {
        mortgageNumMod = "6 et plus"
    }
    else{
        mortgageNumMod = mortgageNum.toString();
    }

    if(houseStatusMod != "Propriétaire (avec crédit immobilier en cours)" && houseStatusMod != "Propriétaire (sans crédit immobilier en cours)") {
        cy.get('input#rentAmount-input').type(rentOrMortgage);
    }
    if(houseStatusMod == "Propriétaire (avec crédit immobilier en cours)") {
        cy.get('input#mortgageAmount-input').type(rentOrMortgage);
    }
    if(childNumMod != "0") {
        cy.get('input#childSupportPaymentsAmount-input').type(suppPayment);
        cy.get('input#childCareExpensesAmount-input').type(childExp);
    }

    cy.get('select#loanCount-input').select(mortgageNumMod);

    if(mortgageNum > 0) {
        for (let i = 0; i <= mortgageNum; i++) {
            cy.get('select#type-input').eq(i).select(mortgageType);
            cy.get('input# loanAmount-input').eq(i).type(mortgageAmount);
        }
    } 
//cy.get('span[data-test="totalOutcome"]').should('contain', total);
},

secondFormBankStep(bank, sinceYear){
    cy.get('select#bankCode-input').select(bank);
    cy.get('input#bankFrom-input-year').type(sinceYear);
},

//-------------------------------------------------------
secondFormPersonalInformation(isPartner, gender, firstName, lastName,  birthDate, birthCity, maidenName ="", birthCountry ="France"){

    var genderLocator =""
    var birthDay = birthDate.split('/')[0];
    var birthMonth = birthDate.split('/')[1];
    var birthYear = birthDate.split('/')[2];


    switch(gender) {
        case "Mr": genderLocator = 'label[for="GENDERCODE_M"]'; break;
        case "Mme" : genderLocator = 'label[for="GENDERCODE_MME"]'; break;
        case "Mlle": genderLocator = 'label[for="GENDERCODE_MLLE"]'; break;   
    }
    cy.wait(2000)
    cy.get(genderLocator).click();
    cy.get('input#lastName-input').type(lastName); 
    cy.get('input#firstName-input').type(firstName); 
    cy.get('input#dateOfBirth-input-day').type(birthDay); 
    cy.get('input#dateOfBirth-input-month').type(birthMonth); 
    cy.get('input#dateOfBirth-input-year').type(birthYear); 

    if(gender== "Mme") {
        cy.get('input#maidenName-input').type(maidenName);
    }

    if(birthCountry != "France") {
        cy.get('select#countryCode-input').select(birthCountry);
    }
    if(isPartner){cy.get('input#partnerLocationOfBirthAutocomplete-input').type(birthCity);}
    else{cy.get('input#locationOfBirthAutocomplete-input').type(birthCity);}
    cy.get('ul > li > span').contains(birthCity).click();

//cy.get('ul > li > span').contains('Je ne trouve pas ma ville').click();
//cy.get('input#postalCode-input').type(); 
//cy.get('select#city-input').click();
},

secondFormAdressStep(cellPhoneNum, country="France", address ="",  phoneNum="") {
    
    cy.get('input#cellPhoneNumber-input').type(cellPhoneNum); 

    if(country != "France") {
        cy.get('select#countryCode-input').select(country);
    }
    if(country == "France") {
        cy.get('input#locationOfBirthAutocomplete-input').type(address);
    }
    if(phoneNum != "") {
        cy.get('input#phoneNumber-input').type(phoneNum); 
    }
},


//-------------------------------------------------------------

checkCreditSimulationSuccessful(isSuccessful) {
    if(isSuccessful == "success") {cy.url().should('contain','https://creditconso.panorabanques.com/resultats');}
    if(isSuccessful == "Failure") { cy.url().should('contain','https://fr-simulation.younited-credit.com/offers/refused_reoptin');
                        cy.get('div#decision-refused-step > h2 > span').should('contain','Nous avons d\'autres solution pour vous');}
},
//cy.url().should('contain','https://fr-simulation.younited-credit.com/offers/refused_reoptin');
//cy.get('div#decision-refused-step > h2 > span').should('contain','Nous avons d\'autres solution pour vous');

//cy.url().should('contain','https://creditconso.panorabanques.com/resultats');

//cy.get('span.pink-bold').should('contain',a);
//cy.get('span.pink-bold').should('contain',b);
//cy.get('div.row-offer').first().should('be.visible');


fillFormData(project, budget, duration, title, lastname, firstname, maidenname, email, relsituation, childnum, housing, housingsince, prosituation, sector, profession, prosince, income, addincome, rentmortgage, childexp, bank, banksincedate) {
    this.startSimulator(project, budget, duration);
    this.firstFormFill(email, relsituation);
    this.secondFormEmailStep(email);
    this.secondFormNextStep();
    this.secondFormSituationStep(childnum);
    this.secondFormNextStep();
    this.secondFormHousingStep(housing, housingsince);
    this.secondFormNextStep();
    this.secondFormProfessionStep(prosituation,sector,profession,prosince);
    this.secondFormNextStep();
    if(relStatusMod == "Marié(e)"|| relStatusMod == "Vie Maritale / PACS") {
        cy.wait(10000);
        this.secondFormPartnerProfessionStep(prosituation,sector,profession,prosince);
        this.secondFormNextStep()}
    this.secondFormIncomeStep(income, income, addincome, addincome);
    this.secondFormNextStep();
    this.secondFormRentMortgageStep(rentmortgage, 0, childexp);
    this.secondFormNextStep();
    this.secondFormBankStep(bank, banksincedate);
    this.secondFormNextStep();
    this.secondFormPersonalInformation(false, title, firstname, lastname, '01/02/1990', 'Grenoble', maidenname);
    this.secondFormNextStep();
    if(relStatusMod == "Marié(e)"|| relStatusMod == "Vie Maritale / PACS") {
        cy.wait(10000);
        this.secondFormPersonalInformation(true, title, firstname, lastname, '01/02/1990', 'Grenoble',maidenname);
        this.secondFormNextStep()}
    this.secondFormAdressStep('0607080910', 'France', '22 rue de la république', '0102030405');
    this.secondFormNextStep();
    
}

}
