
export default {

    gotoPage(pageName){
        var pageURL ="";
        switch(pageName) {
            case "home": pageURL = "https://www.younited-credit.com";break;
            case "investor login": pageURL = "https://investor.younited-credit.com/";break;
            case "customer login": pageURL = "https://customeridentity.younited-credit.com/";break;
            case "account creation": pageURL = "https://customeridentity.younited-credit.com/Fr/Account/CreateNewAccount"; break;
        }
        
        cy.visit(pageURL);
        cy.wait(5000); 
        cy.url().should('contain',pageURL)   

//        if(pageName == "home"){cy.get('button#didomi-notice-agree-button').click()}

    },

    gotoURL(pageURL){
        cy.visit(pageURL);
    },

    checkPageURL(pageURL){
        cy.url().should('eq',pageURL);
    }
}