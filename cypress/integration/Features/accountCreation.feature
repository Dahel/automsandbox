Feature: Account creation

Scenario: Access account creation page
    Given I am on the "customer login" page
    When I am clicking on the account creation button
    Then I am redirected to the account creation form

@tnr
Scenario: New account creation OK
    Given I am on the "account creation" page
    When I am creating a new account
    Then I can see my account had been created

@tnr
Scenario: account creation KO invalid information
    Given I am on the "account creation" page
    When I am creating a new account with bad credentials
    Then I can see errors on the account creation page