@credit
Feature: Credit Simulation

@tnr
Scenario Outline: Credit simulation
   Given I am on the "home" page
    When I am making a new credit simulation with the following informations: <Projet>, <Somme>, <Duree>, <Titre>, <Nom>, <Prenom>, <NomJeuneFille>, <Email>, <Situation>, <NbEnfants>, <Logement>, <LogeDepuis>, <Emploi>, <Secteur>, <Profession>, <EmploiDepuis>, <Salaire>, <AutreRevenu>, <Loyer-Credit>, <ChargesEnfant>, <Banque>, <BanqueDepuis>
    Then I can see my credit simulation is <isSuccessful>

    Examples:
      |                 Projet                 | Somme | Duree   | Titre   | Nom      | Prenom      | NomJeuneFille   | Email          | Situation             | NbEnfants | Logement                                         | LogeDepuis   | Emploi                                     | Secteur   | Profession                                      | EmploiDepuis   | Salaire | AutreRevenu | Loyer-Credit | ChargesEnfant | Banque             | BanqueDepuis   | isSuccessful |
      | "Travaux et amélioration de l'habitat" | 7500  | "12"    | "Mlle"  | "Durant" | "Catherine" | ""              | "oguri@cap.fr" | "Vie Maritale / PACS" | 2         | "Propriétaire (avec crédit immobilier en cours)" | "10/2020"    | "Indépendants / Travailleurs non salariés" | ""        | "Profession libérale médicale et para-médicale" | "12/2016"      | 1500    | 250         | 100          | 150           | "Banque Postale"   | "2010"         | "Success"    |
      | "Véhicule d'occasion"                  | 10500 | "24"    | "Mr"    | "Paul"   | "Dupont"    | ""              | "gld@ship.fr"  | "Célibataire"         | 0         | "Locataire"                                      | "05/2015"    | "CDI"                                      | "Privé"   | "Employé(e) de bureau"                          | "01/2009"      | 3000    | 750         | 350          | 0             | "Caisse d'Epargne" | "1999"         | "Success"    |
      | "Voyage, vacances"                     | 50000 | "84"    | "Mme"   | "Garant" | "Anne"      | "Auguste"       | "test@tkto.fr" | "Veuf(ve)"            | 1         | "Hébergé(e) par un autre membre de la famille"   | "01/2022"    | "Pensionnés / Retraités"                   | ""        | ""                                              | "06/2020"      | 850     | 0           | 0            | 0             | "Société Générale" | "2021"         | "Success"    |