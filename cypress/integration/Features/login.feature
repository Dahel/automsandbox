
@login
Feature: login

#Scenario: Login KO with empty credentials as customer
#    Given I am on the "customer login" page
#    When I try to login with empty fields
#    Then I can see the errors on my empty login fields

#Scenario: Login KO with invalid credentials as customer
#    Given I am on the "customer login" page
#    When I try to login with invalid credentials
#    Then I am can see the login error message

#Scenario: Login KO with email not activated
#    Given I am on the "customer login" page
#    When I try to login with an unactivated email
#    Then I am can see the unactivated mail page

@tnr
Scenario: Login OK with valid credentials as customer
    Given I am on the "customer login" page
    When I try to login with a valid account
    Then I am logged in

@tnr
Scenario: Disconnect
    Given I am on the "customer login" page
    And I am already logged in
    When I am disconnecting
    Then I am disconnected