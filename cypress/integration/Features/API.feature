@API
Feature: API test

Background: Load files
    Given I load my data files

#Scenario: register new user
#    When I register a new user with mail:"abc@def.fr"
#    Then I can see my user "abc@def.fr" is registered

Scenario: User already registered
    When I register a new user with mail:"abc@def.fr"
    Then I can see my user is already registered

@tnr
Scenario: login user OK
    When I login with my user "gld@ship.com"
    Then I can see my user is logged in

Scenario: Login user KO 
    When I login with my user "azertyuiop@zertyuiop.fr"
    Then I can see my user is not logged in

#Scenario: create new product
#    Given I am logged in
#    When I create a new product: "Mototrola Mobile IMAS QWERTY", "Tel Motorola Mobile Sed passurum accepimus filium obscuro non haec crebritate filium fallaciis armatis Histriam iurandi inquit ubi omne inopinum velut prope prope.", "mtrl.jpg", 250
#    Then I can see my product "Mototrola Mobile IMAS QWERTY" has been created

Scenario: Product already exist
    Given I am logged in
    When I create a new product: "Mototrola Mobile IMAS QWERTY", "Tel Motorola Mobile Sed passurum accepimus filium obscuro non haec crebritate filium fallaciis armatis Histriam iurandi inquit ubi omne inopinum velut prope prope.", "mtrl.jpg", 250
    And I create a new product: "Mototrola Mobile IMAS QWERTY", "Tel Motorola Mobile Sed passurum accepimus filium obscuro non haec crebritate filium fallaciis armatis Histriam iurandi inquit ubi omne inopinum velut prope prope.", "mtrl.jpg", 250
    Then I see that my product already exists

Scenario Outline: product creation KO
    Given I am logged in
    When I create a new product: <name>, <description>, <url>, <price>
    Then I see the following response: <error> <message>

    Examples:
        |           name            | description                                                                                               | url           | price | error | message                                                       | 
        | "azerty uiopqs edfvbhuf"  | "azertyu iopazertyu iopazertertyuiop azertyuiopa  zertyui opazertyuz lkjqwertymlkjqwert ymlkjqwertym"     | "abcdef.jpg"  | 10    | 400   | "description must be longer than or equal to 100 characters"  | 
        | "azertyuiopqsdfghjqw"     | "qwertymlkjqwertymlkjqwe rtymlkjqw ertymlkjqwe rtymlkjqwertymlkjqwertym lkjqwertymlkjqwert ymlkjqwertgd"  | "apgy.gif"    | 30    | 400   | "name must be longer than or equal to 20 characters"          | 
        | "qwertyuiopwxcvbn qsdf"   | "azertyuiopaz ertyuiopazertyuio pazertyuiopazertyuiopazertyu iopazertyuiopazertyuio pazertyu iopazertyui" | "qwertyui"    | 40    | 400   | "image must be an URL address"                                |
        | "qwertyuiopwxcvbn qsdf"   | "azertyuiopaz ertyuiopazertyuio pazertyuiopazertyuiopazertyu iopazertyuiopazertyuio pazertyu iopazertyui" | "qwert.com"   | 40    | 403   | "you have invalid url image"                                  |  
        | "azertyuiopqsdfghjklmwst" | "ascfgtyujklm poiuyhgvcxdserthjkuytredfgjsbf zhjefbzhefbqikfbq rgqhsrtjzk qerhqhqhq tq qrtqojhnbvfdertg"  | "wasd.png"    | 0     | 403   | "you have invalid price"                                      | 

Scenario: add comment to product
    Given I am logged in
    When I add a comment "Azertyuiopqsdfghjklm" to my current product
    Then I can see my comment "Azertyuiopqsdfghjklm" has been added

@tnr
Scenario: add single product to cart
    Given I am logged in
    When I add a product to my cart
    Then my product is added to my cart

@tnr
Scenario: update product Qty
    Given I am logged in
    And I have a product in my cart
    When I update my product quantity by 1
    Then I can see my product quantity has been updated

@tnr
Scenario: delete single product from my cart
    Given I am logged in
    And I have a product in my cart
    When I delete a product from my cart
    Then my product has been removed from my cart

@tnr
Scenario: delete all products from my cart
    Given I am logged in
    And I have a product in my cart
    When I delete all products from my cart
    Then all my products have been deleted from my cart
    And I check my cart
    And my cart is now empty

@tnr
Scenario: create order
    Given I am logged in
    And I have a product in my cart
    When I complete my order
    Then my order is completed
    And I check my cart
    And my cart is now empty

Scenario: Order failed cart empty
    Given I am logged in
    When I delete all products from my cart
    And I complete my order
    Then my order completion failed